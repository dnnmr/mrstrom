#!/usr/bin/env bash

# Import util functions
source ../bash_utils/func.sh

get_git_dataset https://github.com/bids-standard/bids-examples.git toy_dataset "*/dwi/*.nii*"

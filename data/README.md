# What is a dataset ?

To be considered a valid dataset, one must contain pairs of diffusion mri 4D images 
with their denoised counterpart. The denoised version will be considered as the ground
truth result for most of the networks used here.

# DATA FORMAT

- Bids derived databases

    Those datasets respect the **bids format**. Each dataset imported must respect the 
    following structure in order to be understood by the dataloader of this code base.

    - DATASET ROOT
        - description.json
        - project-1
            - ...
        - project-2
            - ...
    
    Each project in the dataset must be described in the *description.json* file. The 
    *description.json* file is a simple file containing one object, for which each key
    is a project's name and value the number of usable images in the related project.

- Simulated databases

    Those datasets must either be available as a *tar* archive of a *hdf5* file. When supplied
    in an archive, it must consist of a continuous list of pairs of **data** and **label** (ground
    truth) or a group of folders containing such lists.

# Utilitaries

- Functions are available in *utils.hdf5.driver* to convert datasets to **hdf5** format.
- When done, the datasets will be downloadable from the script *get_data.sh*

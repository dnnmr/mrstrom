from unittest import TestCase
from os.path import join, abspath
from utils.bids.dataloader import BidsDWIDataset


class TestDmriDatasetGenerator(TestCase):
    def setUp(self):
        self.generator = BidsDWIDataset(
            abspath(join("..", "data", "toy_dataset")),
            bvals_ext="bval",
            bvecs_ext="bvec"
        )

    def test_GetItem(self):
        for i in range(len(self.generator)):
            item = self.generator[1]
            assert len(tuple(item)) == 1

        item = self.generator[1]
        assert len(tuple(item)) == 0

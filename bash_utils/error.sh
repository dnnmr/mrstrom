#!/usr/bin/env bash

function exit_with_error_code () {
  code=$1
  message=$2

  echo "[ERROR] Termination error code ($code) : $message"

  return $code
}

function continue_with_warning () {
  message=$1

  echo "[WARNING] $message"
}

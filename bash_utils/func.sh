#!/usr/bin/env bash

# Import error and warning functions
source error.sh

function create_dataset_description () {
  path=$1
  data_regex=$2
  output=$3

  curr=$PWD
  cd "$path" || exit_with_error_code 2 "$path directory does not exists"

  printf "{\n" >> "$output"

  for dataset in */ ; do
    nf=$(find "$dataset" -type f -wholename "$data_regex" | wc -l)
    if [ $nf -gt 0 ]; then
      echo -e "  - found dataset $dataset with $nf admissible samples\n"
      printf "    \"%s\": %d,\n" "${dataset%?}" "$nf" >> "$output"
    fi
  done

  sed -i '$ s/.$//' "$output"
  printf "}\n" >> "$output"

  cd "$curr" || exit_with_error_code 2 "$curr directory does not exists"
}

function get_git_dataset () {
  link=$1
  directory=$2
  data_regex=$3

  curr=$PWD

  git clone "$link" "$directory" || exit_with_error_code 3 "Error while cloning dataset : $link"
  cd "$directory" || exit_with_error_code 2 "$directory directory does not exists"
  echo "Creating dataset description file"
  create_dataset_description "$PWD" "$data_regex" description.json

  cd "$curr" || exit_with_error_code 2 "$curr directory does not exists"
}

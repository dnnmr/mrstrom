

class DMRIDatatype:
    def __init__(self, image, affine, bvals, bvecs, ground_truth):
        self.image = image
        self.affine = affine
        self.bvals = bvals
        self.bvecs = bvecs
        self.ground_truth = ground_truth

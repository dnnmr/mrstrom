from os.path import split


def linearise_tuple(tup):
    return (i for s in tup for i in s)


def split_path(base_path):
    p0, p1 = split(base_path)
    return (split_path(p0) + (p1,)) if len(p1) > 0 else (p1,)

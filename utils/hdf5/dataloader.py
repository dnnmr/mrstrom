import h5py

from torch.utils.data import Dataset
from utils.image import DMRIDatatype
from torch import from_numpy


class HDF5DmriDataset(Dataset):
    """
    Implementation of Pytorch Dataset representing a **diffusion mri**
    dataset encapsulated inside a *HDF5 file*. The HDF5 file must abide
    to the format given by the functions available inside this code
    base **hdf5 driver** (see *utils.hdf5.driver*).

    ...

    Attributes
    ----------
    hdf5_file : string
        path to file (.hdf5) containing the diffusion mri dataset
    cache_len : int
        number of data points to keep in memory
    transform : Transform
        Pytorch transform to apply to the data points

    Methods
    -------
    __getitem__(index)
        Returns an element of the dataset at position *index*
    __len__()
        Returns the number of data points in the dataset

    """
    def __init__(self, hdf5_file, cache_len=3, transform=None):
        super().__init__()
        self.transform = transform

        self.hdf5_file = hdf5_file

        self._cache = {}
        self._infos = []
        self._cache_len = cache_len
        self._get_datasets_infos()

    def __getitem__(self, index):
        data = self._get_data("data", index)

        if self.transform:
            data = self.transform(data)
        else:
            data = from_numpy(data)

        label = from_numpy(self._get_data("label", index))

        return DMRIDatatype(
            data,
            self._get_infos("data")[index]["affine"],
            self._get_infos("data")[index]["bvals"],
            self._get_infos("data")[index]["bvecs"],
            label
        )

    def __len__(self):
        return len(self._get_infos("data"))

    def _get_datasets_infos(self):
        with h5py.File(self.hdf5_file, 'r') as datasets:
            # Go through the dataset and save the information of every item
            for g_tag, group in datasets.items():
                for d_tag, data in group.items():
                    idx = -1
                    if len(self._cache) < self._cache_len:
                        # If there is still place in cache, load the dataset
                        idx = self._add_to_cache(data[()], g_tag)

                    # Create the data point info dict and save it
                    self._infos.append({
                        'group': g_tag,
                        'tag': d_tag,
                        'shape': data[()].shape,
                        'bvals': data.attrs['bvals'],
                        'bvecs': data.attrs['bvecs'],
                        'affine': data.attrs['affine'],
                        'cache': idx
                    })

    def _add_to_cache(self, dataset, group):
        if group not in self._cache:
            self._cache[group] = [dataset]
        else:
            self._cache[group].append(dataset)

        return len(self._cache[group]) - 1

    def _get_data(self, tag, index):
        group = self._get_infos(tag)[index]['group']
        # If present group absent of cache, load it
        if group not in self._cache:
            self._load_dataset(group)
        # Get cache index from infos and return data point
        idx = self._get_infos(tag)[index]['cache']
        return self._cache[group][idx]

    def _get_infos(self, tag):
        return [info for info in self._infos if info['tag'] == tag]

    def _load_dataset(self, gp):
        """Loads all data points belonging to the group *gp*

        """
        with h5py.File(self.hdf5_file, 'r') as datasets:
            for d_tag, data in datasets[gp].items():
                idx = self._add_to_cache(data.value, gp)
                group_idx = next(
                    i for i, v in enumerate(self._infos) if v['group'] == gp)

                self._infos[group_idx + idx]['cache'] = idx

            if len(self._cache) > self._cache_len:
                to_remove = list(self._cache)
                to_remove.remove(gp)
                self._cache.pop(to_remove[0])
                self._infos = [
                    {
                        'group': dt['group'],
                        'tag': dt['tag'],
                        'shape': dt['shape'],
                        'bvals': dt['bvals'],
                        'bvecs': dt['bvecs'],
                        'affine': dt['affine'],
                        'cache': -1
                    } if dt['group'] == to_remove[0] else dt
                    for dt in self._infos
                ]

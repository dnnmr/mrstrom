import os
import glob

import h5py
import nibabel as nib
import numpy as np

from utils.bids.dataloader import BidsDWIDataset
from utils.func import split_path


def convert_simulation_to_hdf5(
        root_dir, hdf5_filename, path_depth=3,
        ground_truth_prefix=None, ground_truth_suffix=None
):
    gt_expr = "{}*{}.nii*".format(
        "[!{}]".format(ground_truth_prefix) if ground_truth_prefix else "",
        "[!{}]".format(ground_truth_suffix) if ground_truth_suffix else "",
    )

    with h5py.File(hdf5_filename, 'w') as hdf5_file:
        for i, data_dir in enumerate(os.listdir(root_dir)):
            if os.path.isdir(os.path.join(root_dir, data_dir)):
                for j, dataset in enumerate(
                        glob.glob(os.path.join(root_dir, data_dir, gt_expr))
                ):
                    group = hdf5_file.create_group(
                        "data_{}_{}".format(data_dir, j)
                    )
                    base_name = dataset.split(".")[0]
                    extension = ".".join(dataset.split(".")[1:])
                    gt_name = os.path.join(
                        os.path.dirname(base_name),
                        "{}.{}".format(
                            "_".join(
                                filter(
                                    lambda s: s,
                                    [
                                        ground_truth_prefix,
                                        os.path.basename(base_name),
                                        ground_truth_suffix
                                    ]
                                )
                            ),
                            extension
                        )
                    )

                    if not os.path.exists(gt_name):
                        hdf5_file.pop("data_{}_{}".format(data_dir, j))
                        continue

                    image = nib.load(dataset)
                    ground_truth = nib.load(gt_name)
                    bvals = np.loadtxt("{}.bvals".format(base_name))
                    bvecs = np.loadtxt("{}.bvecs".format(base_name))

                    attrs_dict = {
                        "filename": os.path.basename(base_name),
                        "root": os.path.join(*(split_path(
                            os.path.dirname(base_name)
                        )[-path_depth:]))
                    }

                    add_dwi_sample_to_hdf5(
                        image.get_fdata(), bvals, bvecs, image.affine,
                        ground_truth.get_fdata(), group, attrs_dict
                    )


def convert_bids_to_hdf5(root_dir, ground_truth_suffix, hdf5_filename):
    bids_data = BidsDWIDataset(root_dir, ground_truth_suffix)

    with h5py.File(hdf5_filename, 'w') as hdf5_file:
        for i in range(len(bids_data)):
            dataset = bids_data[i]
            group = hdf5_file.create_group("data_{}".format(i))
            add_dwi_sample_to_hdf5(
                dataset.image, dataset.bvals, dataset.bvecs, dataset.affine,
                dataset.ground_truth, group
            )


def add_dwi_sample_to_hdf5(
        dwi, bvals, bvecs, affine, ground_truth, group, attrs_dict=None
):
    dwi = group.create_dataset("data", data=dwi)

    dwi.attrs["bvals"] = bvals
    dwi.attrs["bvecs"] = bvecs
    dwi.attrs["affine"] = affine

    if attrs_dict:
        for k, v in attrs_dict.items():
            dwi.attrs[k] = v

    label = group.create_dataset("label", data=ground_truth)
    label.attrs["bvals"] = bvals
    label.attrs["bvecs"] = bvecs
    label.attrs["affine"] = affine


def load_hdf5_database():
    pass

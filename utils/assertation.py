from os.path import exists, join


def assert_file_exists(root, file, message="File does not exist : {}"):
    assert exists(join(root, file)), message.format(join(root, file))


def assert_complete_dmri_dataset(
        root, fname, bvals_ext="bvals", bvecs_ext="bvecs", img_fmt="nii.gz"
):
    unified_error = "Problem with dataset {}{}".format(
        join(root, fname),
        "\n   File does not exist : {}"
    )

    assert_file_exists(root, "{}.{}".format(fname, img_fmt), unified_error)
    assert_file_exists(root, "{}.{}".format(fname, bvals_ext), unified_error)
    assert_file_exists(root, "{}.{}".format(fname, bvecs_ext), unified_error)

from utils.image import DMRIDatatype


def open_dmri_sample(sample, layout, gt_suffix):
    image = sample.get_image()
    ground_truth = layout.get(
        scope='derivatives',
        extension=".nii.gz",
        subject=sample.get_entities(metadata="subject"),
        session=sample.get_entities(metadata="session"),
        datatype=sample.get_entities(metadata="datatype"),
        suffix=gt_suffix,
        return_type='file'
    )[0].get_image()
    return DMRIDatatype(
        image.get_fdata(),
        image.affine,
        layout.get_bval(sample.path),
        layout.get_bvec(sample.path),
        ground_truth.get_fdata()
    )

import torch
import bids
import os

from torch.utils.data import Dataset
from utils.bids.driver import open_dmri_sample
from utils.func import linearise_tuple


# Version of the Pytorch Dataset for diffusion MRI. The getitem
# method takes in an int or a list of ints and loads an image or
# a sample of images at the positions specified.
class BidsDWIDataset(Dataset):
    def __init__(self, project_root, groud_truth_suffix, transform=None):
        self.transform = transform

        self._data = []
        for dataset in next(os.walk(project_root))[1]:
            print(dataset)
            layout = bids.BIDSLayout(os.path.join(project_root, dataset))
            self._data.append(layout.get(
                scope='raw',
                datatype='dwi'
            ))
        self._data = list(linearise_tuple(self._data))
        self._gt_suffix = groud_truth_suffix

    def __len__(self):
        return len(self._data)

    def __getitem__(self, i):
        if torch.is_tensor(i):
            i = i.tolist()

        if i is not list:
            return open_dmri_sample(self._data[i], self._data, self._gt_suffix)

        batch = []
        for ii in i:
            batch.append(
                open_dmri_sample(self._data[ii], self._data, self._gt_suffix)
            )

        return batch

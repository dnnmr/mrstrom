# mrstorm
This is a public repository for a projet for the class *IFT 725 - Neural Networks* at Sherbrooke University. It's subject is the denoising of diffusion mri data using neural networks.

## Requirements

The present package is written in **Python 3.7**. In order to run a full capacity, the user should have a **Nvidia GPU** with **CUDA 10.1** installed.

## Installation

All the python packages required to run the scripts and networks can be installed using

```
pip install -r requirements.txt
```